from django.conf.urls import url
from django.contrib import admin
 
urlpatterns = [
     url(r'^admin/', admin.site.urls),
     url(r'^admin-tool/', admin.site.urls)
 ]
